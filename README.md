# DjPic LLC - PHP with MySQLi

Only supported version of PHP will be updated on the repo.  To view the supported PHP version, visit: [PHP: Supported Versions](https://www.php.net/supported-versions.php)

For more information on this repository, visit: [Docker Image: PHP with MySQLi](https://www.djpic.net/articles/docker-image-php-with-mysqli/)

Prebuilt image available on DockerHub: [djpic/php](https://hub.docker.com/r/djpic/php)

Get notified when images are updated via twitter [@djpic_llc](https://twitter.com/djpic_llc)

## Setup Gitlab Runner for Docker
I use a Gitlab runner to build these images.  Directions on setting up the gitlab running Ubuntu is below.
```shell
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"

dpkg -i gitlab-runner_amd64.deb

gitlab-runner register -n \
--url https://gitlab.com/ \
--registration-token {{TOKEN}} \
--executor docker \
--description "{{RUNNER DESCRIPTION}}" \
--docker-image "docker:dind" \
--docker-privileged \
--docker-volumes "/certs/client"
```