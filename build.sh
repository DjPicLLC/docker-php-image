#!/bin/bash

php_version=$1

if [[ -v php_version ]]; then
  # Build php image with mysqli and memcached extension
  cd MySQLi
  docker build --build-arg php_version=$php_version --tag djpic/php:$php_version-mysqli --tag $CI_REGISTRY_IMAGE/php:$php_version-mysqli .

  # Build image with browscap file
  cd browscap
  wget -O browscap.ini https://browscap.org/stream?q=PHP_BrowsCapINI
  docker build --build-arg php_version=$php_version --tag djpic/php:$php_version-mysqli-browscap --tag $CI_REGISTRY_IMAGE/php:$php_version-mysqli-browscap .
  rm browscap.ini
  wget -O browscap.ini https://browscap.org/stream?q=Full_PHP_BrowsCapINI
  docker build --build-arg php_version=$php_version --tag djpic/php:$php_version-mysqli-full_php_browscap --tag $CI_REGISTRY_IMAGE/php:$php_version-mysqli-full_php_browscap .
  rm browscap.ini
  wget -O browscap.ini https://browscap.org/stream?q=Lite_PHP_BrowsCapINI
  docker build --build-arg php_version=$php_version --tag djpic/php:$php_version-mysqli-lite_php_browscap --tag $CI_REGISTRY_IMAGE/php:$php_version-mysqli-lite_php_browscap .
  rm browscap.ini

  # Docker Push Image
  docker push djpic/php:$php_version-mysqli
  docker push djpic/php:$php_version-mysqli-browscap
  docker push djpic/php:$php_version-mysqli-full_php_browscap
  docker push djpic/php:$php_version-mysqli-lite_php_browscap

  docker push $CI_REGISTRY_IMAGE/php:$php_version-mysqli
  docker push $CI_REGISTRY_IMAGE/php:$php_version-mysqli-browscap
  docker push $CI_REGISTRY_IMAGE/php:$php_version-mysqli-full_php_browscap
  docker push $CI_REGISTRY_IMAGE/php:$php_version-mysqli-lite_php_browscap

  cd ../../
else
  echo "PHP Version Missing"
  exit 1
fi
